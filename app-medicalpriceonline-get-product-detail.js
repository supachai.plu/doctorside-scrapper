const puppeteer = require('puppeteer');
const fs = require('fs'); 
const converter = require('json-2-csv');

const processScrape = async (InputFilename) => {

    let data_list = [];
    const browser = await puppeteer.launch({ 
        headless: true, 
        slowMo: 100, 
        //defaultViewport: null, 
        //args: ['--start-fullscreen', '--disable-notifications', '--disable-infobars', '--mute-audio', '--disable-features=site-per-process'] 
        //args: ['--start-fullscreen',],
        executablePath: "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
        //product: 'firefox'
    });

    const [page] = await browser.pages();
    //await page.on('console', msg => console.log('PAGE LOG:', msg.text()));
    await page.setViewport({width: 1920, height: 1080});
    try{
        const input = InputFilename
        let _list_url = fs.readFileSync( input, { encoding: 'utf-8' }).split('\n')
        const filename =  '___test-'+ input.split('/').pop()

        for( let idx=0 ; idx < _list_url.length ; idx++ ){
            
            
            const link = _list_url[idx].replace(/\r/, '');
            console.log( 'scrap', link )
            //const filename = link.split('/').pop();

            await page.goto( link );
            await page.waitForSelector(".box.bg-default h1");
            //await page.setDefaultNavigationTimeout(0);
        
            const datas = await page.evaluate(() => {

                let r_list = []

                let first_product_detail = {}
                try{
                    first_product_detail['name'] = document.querySelector('.box.bg-default h1').textContent
                    first_product_detail['Price'] = document.querySelector('.box.bg-primary.price big').textContent
                    
                    let details = document.querySelectorAll('div.product .box.bg-default ul.attributes li')
                    for(let idx = 0 ; idx< details.length ; idx++){
                        //console.log( details[idx] )
                        topic = details[idx].querySelector('strong').textContent
                        if( topic ){
                            details[idx].removeChild( details[idx].querySelector('strong') )
                            first_product_detail[topic] = details[idx].textContent
                        }
                    }

                    r_list.push(first_product_detail)
                    
                    const sub_products = document.querySelectorAll('.box.bg-default .product-snippet')
                    if( sub_products.length > 0 ){
                        for(let j = 0 ; j< sub_products.length ; j++){
                            let product_detail = { ...first_product_detail }
                            product_detail['name'] = sub_products[j].querySelector('strong').textContent
                            
                            let details = sub_products[j].querySelectorAll('ul.attributes li')
                            for(let idx = 0 ; idx< details.length ; idx++){
                                //console.log( details[idx] )
                                topic = details[idx].querySelector('strong').textContent
                                if( topic ){
                                    details[idx].removeChild( details[idx].querySelector('strong') )
                                    product_detail[topic] = details[idx].textContent
                                }
                            }
                            r_list.push(product_detail)
                        }
                    }
                    
                }catch(e){
                    console.error('error', e)
                }finally{
                    console.log('-------------- Done')
                }

                return r_list
            });
            
            console.log(datas)

            data_list.push(...datas)   

            converter.json2csv(data_list, (err, csv) => {
                if (err) {
                    throw err;
                }
                console.log(csv);
                fs.writeFileSync( filename + '.csv', csv);
            
                // print CSV string
            }, { emptyFieldValue: '', sortHeader: true, });
            
        }

    }catch(e){console.error(e)
    }finally{
        await page.screenshot({path: 'example.png'});
        await browser.close();
    }
}

const Folder = './medicalpriceonline'
fs.readdirSync(Folder).forEach(file => {
    if( file.indexOf('.txt') > -1 ){
        processScrape(Folder+'/'+file)
        /*
        txt.push(file)
        fs.readFile( Folder + file , function(err, buf) {
            let data = buf.toString()
            let d1 = data.replace(/,/g, '\n')
            console.log(d1);
            fs.writeFile( Folder + file, d1, (err) => {
                if (err) console.log(err);
                console.log("Successfully Written to File.");
            });
        });
        */
    }
});