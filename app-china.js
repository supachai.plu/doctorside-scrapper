const puppeteer = require('puppeteer');
const fs = require('fs'); 
const converter = require('json-2-csv');

(async () => {
    let list = [];
    let data_list = [];
    const browser = await puppeteer.launch({ 
        headless: true, 
        slowMo: 100, 
        //defaultViewport: null, 
        //args: ['--start-fullscreen', '--disable-notifications', '--disable-infobars', '--mute-audio', '--disable-features=site-per-process'] 
        //args: ['--start-fullscreen',],
        executablePath: "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
        //product: 'firefox'
    });

    const [page] = await browser.pages();
    //await page.on('console', msg => console.log('PAGE LOG:', msg.text()));
    await page.setViewport({width: 1920, height: 1080});
try{   
    
    let file_url = '__china_cats.txt'
    
    let _list_url = fs.readFileSync( file_url, { encoding: 'utf-8' }).split('\n')

    for( let idx=0 ; idx < _list_url.length ; idx++ ){
        
        console.log( 'scrap', _list_url[idx] )

        const link = _list_url[idx].replace(/\r/, '');
        const f = link.split('/')
        //const filename = link.split('/').pop();
        let filename = f[ f.length -1 ].length > 0 ? f.pop() : (f.pop(), f.pop())

        await page.goto( link );
        //await page.waitFor(1000);
        await page.setDefaultNavigationTimeout(0);
        //await page.waitForNavigation({waitUntil: "domcontentloaded"})

        let all_url_list = []

        while(true){

            all_url_list = []

            console.log( 'scrap', page.url() )

            let product_url_list = await page.evaluate(() => {
                let url_list = []
                try{
                    let product_list = document.querySelectorAll('.list-node')
                    for( let idx=0 ; idx < product_list.length ; idx++ ){
                        url_list.push( product_list[idx].querySelector('a').href )
                    }
                }catch(e){
                }finally{
                    return url_list
                }
            })
    
            //console.log( product_url_list )
            all_url_list.push( ...product_url_list )
           
            try{
                const hasNextPage = await page.$eval('.page-num a.next', (button) => {
                    return button.href;
                });
                if( hasNextPage ){

                    fs.appendFileSync( filename + '.txt', all_url_list.toString().replace(/,/g, '\n') );
                    await delay(3000)
                    await page.click( '.page-num a.next' )
                }else{
                    break
                }
            }catch(e){
                break;
            }
        }

        //console.log( all_url_list )

        //get last page
        //document.querySelectorAll('ul.page-numbers li')[ document.querySelectorAll('ul.page-numbers li').length -1 ].querySelector('a').href
        //fs.writeFileSync( filename + '.txt', all_url_list.toString().replace(/,/g, '\n') );

    }

}catch(e){console.error(e)
}finally{
    await page.screenshot({path: 'example.png'});
    await browser.close();
}
})()





function delay(time) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, time)
    });
 }

/*
<a href="//www.made-in-china.com/catalog/item999i132/Analysis-Instrument-1156.html" class="next" rel="nofollow">Next<i class="icon"></i></a>

<a class="next page-dis" rel="nofollow">Next<i class="icon"></i></a>

*/