const {Builder, By, Key, until, Options} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

// Input capabilities
var capabilities = {
    'os_version' : '10',
    'resolution' : '1920x1080',
    'browser' : 'chrome',
    'browser_version' : '85.0',
    'os' : 'Windows',
}

//'./driver/chromedriver.exe' 
(async function example() {
  let driver = await new Builder().forBrowser('chrome').withCapabilities(capabilities).build();
  try {
    await driver.get('https://www.google.co.th/');
      await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
      await driver.wait(until.titleIs('webdriver - Google Search'), 1000);
      /*
      await driver.get('http://process.gprocurement.go.th/');
    await driver.findElement(By.id('home-popup-close-button')).click()
    await driver.findElement(By.className('.btn-org.ff')).click()
    */
  } finally {
    await driver.quit();
  }
})();