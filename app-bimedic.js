const puppeteer = require('puppeteer');
const fs = require('fs'); 
const converter = require('json-2-csv');

(async () => {
    let list = [];
    let data_list = [];
    const browser = await puppeteer.launch({ 
        headless: true, 
        slowMo: 100, 
        //defaultViewport: null, 
        //args: ['--start-fullscreen', '--disable-notifications', '--disable-infobars', '--mute-audio', '--disable-features=site-per-process'] 
        //args: ['--start-fullscreen',],
        executablePath: "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
        //product: 'firefox'
    });

    const [page] = await browser.pages();
    //await page.on('console', msg => console.log('PAGE LOG:', msg.text()));
    await page.setViewport({width: 1920, height: 1080});
try{
    let _list_url = fs.readFileSync('_list', { encoding: 'utf-8' }).split('\n')

    for( let idx=0 ; idx < _list_url.length ; idx++ ){
        
        console.log( 'scrap', _list_url[idx].replace(/\r/, '') )

        const link = _list_url[idx].replace(/\r/, '');
        const filename = link.split('/').pop();

        await page.goto( link );
        await page.waitFor(1000);

        let url_list = []
        let count = 0;
        let last_page = await page.evaluate(() => {
            try{
                console.log( parseInt(document.querySelector('.b-myads-pagination li:last-child').getAttribute('data-page-num')) )
                return parseInt(document.querySelector('.b-myads-pagination li:last-child').getAttribute('data-page-num'))
            }catch(e){
                return 1
            }
        })
        await page.waitFor(1000);

        console.log( 'LAST_PAGE', last_page )

        while( count < last_page ){
            await page.goto(`${link}/page-${count++}`);
            await page.waitFor(2000);

            let result = await page.evaluate(() => {
                let r_list = []
                
                let result_list = document.querySelectorAll('ul.b-product-list.sjs-advert-list.m-search-page li.e-product-item.sjs-advert-view')
                /*
                for( let idx = 1 ; idx <= result_list.length ; idx++ ){
                    let r = {}
                    try{
                        let product_condition = result_list[idx].querySelector('.e-short-description-product-item.scss-product-list-desktop').innerText
                        console.log('product_condition', product_condition)
                        //if( product_condition.indexOf('New') > -1 ){
                            /*
                            console.log( result_list[idx].querySelector('.b-product-relinking').innerText )
                            console.log( result_list[idx].getElementsByTagName('h3')[0].innerText )
                            console.log( result_list[idx].querySelectorAll('.e-short-description-product-item.scss-product-list-desktop .e-short-description-product-item-value')[1].innerText )
                            console.log( result_list[idx].querySelector('.e-product-cost').innerText )
                            * /
                            try{
                                r.condition = product_condition
                            }catch(e){}
                            try{
                                r.lb = result_list[idx].querySelector('.b-product-relinking').innerText
                            }catch(e){}
                            try{
                                r.name = result_list[idx].getElementsByTagName('h3')[0].innerText
                            }catch(e){}
                            try{
                                r.year = result_list[idx].querySelectorAll('.e-short-description-product-item.scss-product-list-desktop .e-short-description-product-item-value')[1].innerText
                            }catch(e){}
                            try{
                                r.cost = result_list[idx].querySelector('.e-product-cost').innerText
                            }catch(e){}
                            r_list.push(r)
                        //}
                    }catch(e){}
                }
                */
                
                for( let idx = 1 ; idx <= result_list.length ; idx++ ){
                    try{
                        console.log('result_list[idx]', result_list[idx])
                        let url = (result_list[idx].querySelector('.e-product-image.sjs-quick-open-link')).getAttribute('data-href')
                        console.log('url', url)
                        r_list.push(url)
                    }catch(e){}
                }

                return r_list
            })

            list.push(...result)       
        }
        
        console.log('list', list)

        for( idx = 0 ; idx < list.length ; idx ++ ){
            //if( idx === 5 ) break
            console.log( 'execute', list[idx] )
            await page.goto( list[idx] );
            await page.waitFor(1000);
            let result = await page.evaluate(() => {
                let r_list = []
                console.log( 'Start evaluate' )
                try{
                    let price = document.querySelector('.b-advert-price tr td').textContent.trim().replace(/ /g,'')
                    r_list.push({ 'price': price })
                }catch(e){}

                let data_row = document.querySelectorAll('table.b-advert-table tbody tr')
                console.log( data_row.length )
                for( let row_idx=0; row_idx < data_row.length ; row_idx++ ){
                    let data_col = data_row[row_idx].querySelectorAll('td')
                    console.log( data_col.length )
                    r_list.push({ [data_col[0].textContent.trim()] : data_col[1].textContent.trim() })
                }
                //r_list.push({ url: list[idx] })
                console.log( 'Finish evaluate' )
                return r_list
            })
            result = [ { url_link : list[idx].toString() }, ...result ]
            data_list.push(result)
        }
        console.log('data_list', data_list)
        
        /*
        if( !fs.existsSync('bemedic') ){
            fs.mkdirSync('bemedic')
        }
        converter.json2csv(list, (err, csv) => {
            if (err) {
                throw err;
            }
            console.log(csv);
            fs.writeFileSync( './bimedic/'+ filename + '.csv', csv);
        
            // print CSV string
        });
        */
        converter.json2csv(data_list, (err, csv) => {
            if (err) {
                throw err;
            }
            console.log(csv);
            fs.writeFileSync( filename + '.csv', csv);
        
            // print CSV string
        }, { emptyFieldValue: '', sortHeader: true, });
    }

}catch(e){console.error(e)
}finally{
    await page.screenshot({path: 'example.png'});
    await browser.close();
}
})()