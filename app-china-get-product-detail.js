const puppeteer = require('puppeteer');
const fs = require('fs'); 
const converter = require('json-2-csv');

let datas = []
let filename
  
(async () => {

    const browser = await puppeteer.launch({ 
        headless: true, 
        slowMo: 100, 
        //defaultViewport: null, 
        //args: ['--start-fullscreen', '--disable-notifications', '--disable-infobars', '--mute-audio', '--disable-features=site-per-process'] 
        //args: ['--start-fullscreen',],
        executablePath: "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
        //product: 'firefox'
    });

    const [page] = await browser.pages();
    //await page.on('console', msg => console.log('PAGE LOG:', msg.text()));
    await page.setViewport({width: 1920, height: 1080});
try{   
    
    let file_url = './china/Body-Fluid-Processing-Circulation-Device.html.txt'
    let _list_url = fs.readFileSync( file_url, { encoding: 'utf-8' }).split('\n')
    console.log( '===> ', _list_url.length )

    filename = file_url.split('/').pop()
    
    for( let idx=0 ; idx < _list_url.length ; idx++ ){
        
        console.log( 'scrap', _list_url[idx] )

        const link = _list_url[idx].replace(/\r/, '');
        const f = link.split('/')
        //const filename = link.split('/').pop();
        //let filename = f[ f.length -1 ].length > 0 ? f.pop() : (f.pop(), f.pop())

        await page.goto( link );
        //await page.waitFor(1000);
        await page.setDefaultNavigationTimeout(0);
        //await page.waitForNavigation({waitUntil: "domcontentloaded"})
        
        let product_detail = await page.evaluate(() => {
            let product_detail = {}
            try{
                try{
                    let basicInfo = document.querySelectorAll('.sr-layout-block.bsc-info .basic-info-list .bsc-item')
                    for( let idx = 0 ; basicInfo.length ; idx++){
                        let label = basicInfo[idx].querySelector('.bac-item-label').textContent.trim()
                        product_detail[label] = basicInfo[idx].querySelector('.bac-item-value').textContent.trim()
                    }
                }catch(e){}

                //let product_detail = {}
                try{
                    let attrInfo = document.querySelectorAll('.sr-proMainInfo-baseInfo-propertyAttr tr')
                    for( let idx = 0 ; attrInfo.length ; idx++){
                        let label = attrInfo[idx].querySelector('th').textContent.trim()
                        product_detail[label] = attrInfo[idx].querySelector('td').textContent.trim()
                    }
                }catch(e){}
                //console.log(product_detail)

                //let product_detail = {}
                try{
                    let pricesInfo = document.querySelectorAll('.sr-proMainInfo-baseInfo-propertyPrice tr')
                    for( let idx = 0 ; pricesInfo.length ; idx++){
                        let priceInfo = pricesInfo[idx].querySelectorAll('td')
                        //console.log(pricesInfo.length, priceInfo.length)
                        if( priceInfo.length > 0 ){
                            let label = priceInfo[0].textContent.trim()
                            product_detail[label] = priceInfo[1].textContent.trim()
                        }
                    }
                }catch(e){}
                //console.log(product_detail)

            }catch(e){
            }finally{
                return product_detail
            }
        })

        product_detail['url'] = link

        console.log( '===>', product_detail )
        datas.push(product_detail)
        await delay(5000)
    }
    
    writeToFile(filename, datas)

}catch(e){
    console.error(e)
}finally{
    await page.screenshot({path: 'example.png'});
    await browser.close();
}
})()


function writeToFile(filename, data_list){
    converter.json2csv(data_list, (err, csv) => {
        if (err) {
            throw err;
        }
        console.log(csv);
        fs.appendFileSync( '___detail_'+filename + '.csv', csv);
    
        // print CSV string
    }, { emptyFieldValue: '', sortHeader: true, });
}


function delay(time) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, time)
    });
 }

/*
<a href="//www.made-in-china.com/catalog/item999i132/Analysis-Instrument-1156.html" class="next" rel="nofollow">Next<i class="icon"></i></a>

<a class="next page-dis" rel="nofollow">Next<i class="icon"></i></a>

*/